#!/usr/local/bin/python3.6
#
# Copyright (c) 2018-2019 Intel Corporation
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.

import os
import re
import subprocess
import sys

YEARS=          ['2016', '2017', '2018', '2019']
QUARTERS=       [('Q1', ['--since', '{year}-01-01 00:00:00 +0000',
                         '--until', '{year}-03-31 23:59:59 +0000']),
                 ('Q2', ['--since', '{year}-04-01 00:00:00 +0000',
                         '--until', '{year}-06-30 23:59:59 +0000']),
                 ('Q3', ['--since', '{year}-07-01 00:00:00 +0000',
                         '--until', '{year}-09-30 23:59:59 +0000']),
                 ('Q4', ['--since', '{year}-10-01 00:00:00 +0000',
                         '--until', '{year}-12-31 23:59:59 +0000']),
                ]
PATHS=          [('kernel', 'sys'), ('userspace', ':(exclude)sys')]
GIT_STATS=      os.path.join(os.path.dirname(__file__), 'git-stats.py')

def main(*args):
    res = {}
    for year in YEARS:
        for quarter, q_args in QUARTERS:
            for path, p_arg in PATHS:
                q_args = [i.format(year=year) for i in q_args]
                out = subprocess.check_output([sys.executable, GIT_STATS] +
                                              q_args + ['--', p_arg],
                                              encoding='utf-8').strip()
                if out == '':
                    continue
                out = re.sub('^', '%s_%s,%s,' % (year, quarter, path), out, flags=re.MULTILINE)
                print(out)

if __name__ == '__main__':
    sys.exit(main(*sys.argv[1:]))
