#!/usr/local/bin/python3.6
#
# Copyright (c) 2018-2019 Intel Corporation
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.

from datetime import datetime, timedelta
import re
import subprocess
import sys
import unicodedata

NON_SOURCE_FILES = [ 'sys/cddl/contrib/opensolaris/uts/common/sys/u8_textprep_data.h'
                   , 'sys/contrib/dev/acpica/changes.txt'
                   , 'sys/contrib/dev/ath/ath_hal/ar9300/osprey_reg_map_macro.h'
                   , 'sys/contrib/dev/ath/ath_hal/ar9300/scorpion_reg_map_macro.h'
                   , 'sys/dev/bnxt/hsi_struct_def.h'
                   , 'sys/dev/bxe/57710_init_values.c'
                   , 'sys/dev/bxe/57711_init_values.c'
                   , 'sys/dev/bxe/57712_init_values.c'
                   , 'sys/dev/cxgbe/common/t4_regs.h'
                   , 'sys/dev/ispfw/asm_2400.h'
                   , 'sys/dev/ispfw/asm_2500.h'
                   , 'sys/dev/pms/RefTisa/sallsdk/hda/64k/aap18008.h'
                   , 'sys/dev/pms/RefTisa/sallsdk/hda/64k/iop8008.h'
                   , 'sys/dev/pms/RefTisa/sallsdk/hda/64k/iop8070.h'
                   , 'sys/dev/pms/RefTisa/sallsdk/hda/64k/iopimg.h'
                   , 'sys/dev/pms/RefTisa/sallsdk/hda/64k/raae8070.h'
                   , 'sys/dev/qlnx/qlnxe/ecore_init_values.h'
                   , 'sys/dev/qlnx/qlnxe/reg_addr.h'
                   , 'sys/dev/qlxgbe/ql_fw.c'
                   , 'sys/dev/sfxge/common/efx_regs_mcdi.h'
                   ]

def strnorm(x):
    return unicodedata.normalize('NFKD', x.casefold())

def daterange(start, end):
    return (datetime.strptime(start, '%Y-%m-%d'),
            datetime.strptime(end, '%Y-%m-%d') + timedelta(1) - timedelta.resolution)

def dateinrange(date, daterange):
    start, end = daterange
    return date >= start and date <= end

def getsponsor(commit):
    email = strnorm(commit['email'])
    if email not in KNOWN_SPONSORS:
        return None
    sponsors = KNOWN_SPONSORS[email]
    if type(sponsors) is str:
        return [sponsors]
    for daterange, sponsor in sponsors:
        if dateinrange(commit['time'], daterange):
            return [sponsor]
    return None

KNOWN_SPONSORS =        \
        { strnorm('ae@FreeBSD.org'):            'Yandex'
        , strnorm('alc@FreeBSD.org'):           'Rice University'
        , strnorm('andrew@FreeBSD.org'):        'University of Cambridge'
        , strnorm('asomers@FreeBSD.org'):       'Spectra Logic'
        , strnorm('avg@FreeBSD.org'):           'iXsystems'
        , strnorm('bdrewery@FreeBSD.org'):      'Isilon'
        , strnorm('br@FreeBSD.org'):            'University of Cambridge'
        , strnorm('brooks@FreeBSD.org'):        'SRI'
        , strnorm('cem@FreeBSD.org'):           'Isilon'
        , strnorm('emaste@FreeBSD.org'):        'The FreeBSD Foundation'
        , strnorm('erj@FreeBSD.org'):           'Intel'
        , strnorm('gjb@FreeBSD.org'):           'The FreeBSD Foundation'
        , strnorm('glebius@FreeBSD.org'):       'Netflix'
        , strnorm('hselasky@FreeBSD.org'):      'Mellanox'
        , strnorm('imp@FreeBSD.org'):           'Netflix'
        , strnorm('jeb@FreeBSD.org'):           'Intel'
        , strnorm('kib@FreeBSD.org'):           'The FreeBSD Foundation'
        , strnorm('markj@FreeBSD.org'):         'Isilon'
        , strnorm('mav@FreeBSD.org'):           'iXsystems'
        , strnorm('np@FreeBSD.org'):            'Chelsio Communications'
        , strnorm('rwatson@FreeBSD.org'):       'University of Cambridge'
        , strnorm('sbruno@FreeBSD.org'):        'Limelight'
        , strnorm('scottl@FreeBSD.org'):        'Netflix'
        , strnorm('sephe@FreeBSD.org'):         'Microsoft'
        }

SPONSOR_FIXUPS =        \
        { 'ABT Systems Lrd':                    'ABT Systems Ltd'
        , 'ABT systems Ltd':                    'ABT Systems Ltd'
        , 'ABT Systmes Ltd':                    'ABT Systems Ltd'
        , 'Amazoncom':                          'Amazon'
        , 'Broadcom Limited':                   'Broadcom'
        , 'Cavium':                             'Cavium Networks'
        , 'Chelsio communications':             'Chelsio Communications'
        , 'Chelsio Communicatons':              'Chelsio Communications'
        , 'Citrix System R&D':                  'Citrix Systems R&D'
        , 'Citrix systems R&D':                 'Citrix Systems R&D'
        , 'DAPRA':                              'DARPA'
        , 'DARPA / AFRL':                       'DARPA/AFRL'
        , 'DARPA, AFRL':                        'DARPA/AFRL'
        , 'Dell EMC Isilon':                    'Dell EMC'
        , 'Dell Technologies':                  'Dell EMC'
        , 'Dell':                               'Dell EMC'
        , 'Dell/EMC Isilon':                    'Dell EMC'
        , 'EMC / Isilon Storage Division':      'Dell EMC'
        , 'EMC Dell Isilon':                    'Dell EMC'
        , 'Google Summer of Code 2017':         'Google'
        , 'Google Summer of Code':              'Google'
        , 'HardendBSD':                         'HardenedBSD'
        , 'Isilon':                             'Dell EMC'
        , 'iX Systems':                         'iXsystems'
        , 'Juniper':                            'Juniper Networks'
        , 'Klara':                              'Klara Systems'
        , 'Limeligght Networks':                'Limelight Networks'
        , 'Limelight  Networks':                'Limelight Networks'
        , 'LimeLight Networks':                 'Limelight Networks'
        , 'Limelight':                          'Limelight Networks'
        , 'Mellanox':                           'Mellanox Technologies'
        , 'Microchip':                          'Microchip Technology'
        , 'Microsoft OSTC':                     'Microsoft'
        , 'Neflix':                             'Netflix'
        , 'Netfix':                             'Netflix'
        , 'NetFlix':                            'Netflix'
        , 'netflix':                            'Netflix'
        , 'panzura':                            'Panzura'
        , 'Rubicon Communication':              'Rubicon Communications'
        , 'Rubicon Comunications':              'Rubicon Communications'
        , 'Rubiconn Communications':            'Rubicon Communications'
        , 'the FreeBSD Foundation':             'The FreeBSD Foundation'
        }

def groupcommits(lines):
    acc = []
    for l in lines:
        if l.startswith('commit '):
            if acc:
                yield acc
            acc = [l]
        else:
            acc.append(l)
    if acc:
        yield acc

AUTHOR_RE =     re.compile(r'''
        ^
        author                  \s+
        (?P<name>[^<]+)         \s+
        <(?P<email>[^>]+)>      \s+
        (?P<timestamp>\d+)      \s+
        (?P<timezone>[+-]\d+)
        $''', re.VERBOSE)
PATH_RE =       re.compile(r'''
        ^
        (?P<added>\d+)          \s+
        (?P<removed>\d+)        \s+
        (?:\{.*?=>\s+(?P<path1>[^}]+)})?
        (?P<path2>.*)
        $''', re.VERBOSE)
def parsecommit(lines):
    (name, email, timestamp, timezone) = next(filter(lambda x:x, map(AUTHOR_RE.match, lines))).groups()
    message = ''.join(map(lambda x:x[4:], filter(lambda x:x.startswith('   '), lines)))
    paths = []
    for f in filter(lambda x:x, map(PATH_RE.match, lines)):
        (add, rem, path1, path2) = f.groups()
        path = (path1 if path1 else '') + path2
        if path.endswith('.uu'): continue
        if path in NON_SOURCE_FILES: continue
        paths.append({'insertion':int(add), 'deletion':int(rem), 'path':path})
    return {'email':email, 'time':datetime.utcfromtimestamp(int(timestamp)), 'body':message, 'paths':paths}

SUBMITTED_BY_RE =       re.compile(r'''
        ^
        Submitted \s+ by:       \s+
        (.*?(?:<([^>]+)>)?)     \s*
             # ~~~~~~~~~
             # grab an email if there is one

        (?:\([^)]*\)\.?)?       \s*
         # ~~~~~~~~~
         # optionally ignore parenthesized comment
        $''', re.VERBOSE | re.MULTILINE)
def findauthor(commit):
    sb = SUBMITTED_BY_RE.findall(commit['body'])
    if sb:
        full, mail = sb[0]
        if mail:
            mail = mail.replace(' at ', '@')
            mail = mail.replace(' AT ', '@')
            mail = mail.replace(' dot ', '.')
            mail = mail.replace(' DOT ', '.')
            mail = mail.replace('_', '@', 1)
            mail = mail.replace(' ', '@', 1)
            mail = mail.replace(' ', '.')
            commit['email'] = mail
        else:
            full = full.replace(' at ', '@')
            full = full.replace(' AT ', '@')
            full = full.replace(' dot ', '.')
            full = full.replace(' DOT ', '.')
            commit['email'] = full
    if re.match('^[0-9a-z]+$', commit['email']):
        commit['email'] += '@FreeBSD.org'
    if commit['email'].endswith('@'):
        commit['email'] += 'FreeBSD.org'
    return commit

SPONSORED_BY_RE =       re.compile(r'''
        ^
        Sponsored \s+ by: \s+ (.*)
        $''', re.VERBOSE | re.MULTILINE)
INC_RE =                re.compile(r'''
        ,?\s*
        (?: inc(?:orporated)?
        |   corp(?:oration)?
        |   llc
        |   \.
        )
        ''', re.VERBOSE | re.IGNORECASE)
COMMENT_RE =            re.compile(r'''
        \s* \([^()]*\)
        ''', re.VERBOSE)
SPLIT_RE =              re.compile(r'''
        (?:,|;|\s*\band\b)\s*
        ''', re.VERBOSE | re.IGNORECASE)
def sponsors(commit):
    body = COMMENT_RE.sub('', commit['body'])
    sponsors = SPONSORED_BY_RE.findall(body)
    if not sponsors:
        sponsors = getsponsor(commit)
    if not sponsors:
        sponsors = ['Independent']

    sponsors = [INC_RE.sub('', s) for s in sponsors]
    sponsors = [SPONSOR_FIXUPS[s] if s in SPONSOR_FIXUPS else s for s in sponsors]
    sponsors = [x for s in sponsors for x in SPLIT_RE.split(s)]
    sponsors = [SPONSOR_FIXUPS[s.strip()] if s.strip() in SPONSOR_FIXUPS else s.strip() for s in sponsors]
    commit['sponsors'] = sponsors
    return commit

def main(*args):
    cmd = ['git', 'log', '--pretty=raw', '--numstat', '--show-notes'] + list(args)
    proc = subprocess.Popen(cmd, stdin=subprocess.DEVNULL,
                            stdout=subprocess.PIPE, encoding='utf-8')
    lines = {}
    commits = {}
    lines_total = 0
    commits_total = 0
    for c in groupcommits(proc.stdout):
        c = sponsors(findauthor(parsecommit(c)))
        loc = sum(p['insertion'] + p['deletion'] for p in c['paths'])
        if 'svn path=/head/;' not in c['body'] or loc > 100000:
            # filter out vendor imports, mass deletes, etc
            continue
        for s in c['sponsors']:
            lines[s] = lines.get(s, 0) + loc
            lines_total += loc
            commits[s] = commits.get(s, 0) + 1
            commits_total += 1
    lines = sorted(lines.items(), key=lambda x:-x[1])
    commits = sorted(commits.items(), key=lambda x:-x[1])

    for n, ls in lines:
        print('lines_changed,%s,%d,%f' % (n, ls, ls/lines_total))

    for n, cs in commits:
        print('commits,%s,%d,%f' % (n, cs, cs/commits_total))

if __name__ == '__main__':
    sys.exit(main(*sys.argv[1:]))
